About design
============

 * Primarly the code should be clear...
 * ... and modular.
 * I don't care if the implementation is optimized to the limits.

Interface
=========

See headers.

For examples you can take a look at `hash_map_test.c`.

Testing?
========

Not a very exhaustive test but any.

    make
    ./hash_map_test
