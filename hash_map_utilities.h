#ifndef HASH_MAP_UTILITIES_H
#define HASH_MAP_UTILITIES_H

#include <stdbool.h>
#include <stddef.h>
#include "hash_map.h"

/**
 * hash map with null-terminated strings as keys
 */
extern HashMap* string_key_hash_map_new();

extern HashMapKeyHashFunction string_hash_function;
extern HashMapKeyEqualFunction string_equal_function;
extern HashMapKeyCopyFunction string_copy_function;

/**
 * just free key using given free function
 */
extern HashMapKeyFreeFunction hash_map_key_free_function_simple;

/**
 * hash map with pointers as keys
 */
extern HashMap* pointer_key_hash_map_new();
extern HashMap* pointer_key_custom_hash_map_new(HashMapMallocFunction*, HashMapFreeFunction*);

extern HashMapKeyHashFunction pointer_hash_function; /* ptr >> 2 */
extern HashMapKeyEqualFunction pointer_equal_function; /* == */
extern HashMapKeyCopyFunction pointer_copy_function; /* identity */
extern HashMapKeyFreeFunction pointer_free_function; /* do nothing */

#endif
