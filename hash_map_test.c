#include "hash_map.h"
#include "hash_map_utilities.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

static void test_a() {
	size_t n = 4;
	const char* keys[] = {
		"a rolling stone",
		"haste makes",
		"ignorance",
		"POSIX"
	};
	const char* values[] = {
		"gathers no moss",
		"waste",
		"is bliss",
		"me harder"
	};
	
	HashMap* map = string_key_hash_map_new();
	
	printf("### inserting proverbs ###\n");
	for (size_t i = 0; i < n; i ++) {
		printf("\"%s\", \"%s\"\n", keys[i], values[i]);
		hash_map_set(map, keys[i], strdup(values[i]));
	}
	
	printf("### checking map contents ###\n");
	for (size_t i = 0; i < n; i ++) {
		printf(
			"map[\"%s\"] = \"%s\"\n",
			keys[i],
			(char*)hash_map_get(map, keys[i])
		);
	}
	
	printf("### deleting key \"%s\" ###\n", keys[n - 1]);
	free(hash_map_get(map, keys[n - 1]));
	hash_map_remove(map, keys[n - 1]);
	
	printf("### checking map contents ###\n");
	for (size_t i = 0; i < n; i ++) {
		printf(
			"map[\"%s\"] = \"%s\"\n",
			keys[i],
			(char*)hash_map_get(map, keys[i])
		);
	}
	
	printf("### deleting other keys ###\n");
	for (size_t i = 0; i < n - 1; i ++) {
		free(hash_map_get(map, keys[i]));
		hash_map_remove(map, keys[i]);
	}
	
	hash_map_free(map);
}

int main(){
	test_a();
}
