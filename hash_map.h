#ifndef HASHMAP_H
#define HASHMAP_H

#include <stdbool.h>
#include <stddef.h>

typedef struct _HashMap HashMap;

/**
 * callback types
 */
typedef void* (HashMapMallocFunction)(size_t);
typedef void (HashMapFreeFunction)(void*);
typedef size_t (HashMapKeyHashFunction)(void const*);
typedef bool (HashMapKeyEqualFunction)(void const*, void const*);
typedef void* (HashMapKeyCopyFunction)(void const*, HashMapMallocFunction*);
typedef void (HashMapKeyFreeFunction)(void*, HashMapFreeFunction*);

/**
 * creates new hashmap
 */
extern HashMap* hash_map_new(
	float load_factor, /* number of stored elements to container size ratio */
	size_t minimal_size,
	
	HashMapMallocFunction*, /* function for allocating memory */
	HashMapFreeFunction*, /* function for freeing memory */
	
	HashMapKeyHashFunction*, /* function used to compute hash from keys */
	HashMapKeyEqualFunction*, /* function to check if keys are equal */
	HashMapKeyCopyFunction*, /* function used to copy the key (to prevent external key changes) */
	HashMapKeyFreeFunction* /* function used to free copied keys when they are not needed anymore */
);
extern void hash_map_free(HashMap*);

extern bool hash_map_has(HashMap const*, void const* key);
extern void hash_map_set(HashMap*, void const* key, void* value);
extern void* hash_map_get(HashMap const*, void const* key);
extern void hash_map_remove(HashMap*, void const* key);
extern size_t hash_map_elements_count(HashMap const*);

/**
 * call function for every value in a hash map
 *
 * you shouldn't change hash map inside callback
 */
typedef void (HashMapForEveryFunction)(void const* key, void* value, void* context_data);
extern void hash_map_for_every(HashMap*, HashMapForEveryFunction*, void* context_data);

#endif
