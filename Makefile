CC = gcc
CFLAGS = -Wall -Wextra --pedantic -std=gnu99
LDFLAGS =

all: hash_map_test hash_map.o hash_map_utilities.o

hash_map_test: hash_map_test.o hash_map.o hash_map_utilities.o
	$(CC) $(LDFLAGS) -o $@ $^

clean:
	rm -rf hash_map_test *.o
