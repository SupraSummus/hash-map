#include "hash_map_utilities.h"
#include <string.h>
#include <stdlib.h>

HashMap* string_key_hash_map_new() {
	return hash_map_new(
		0.75, 16,
		malloc, free,
		
		string_hash_function,
		string_equal_function,
		string_copy_function,
		hash_map_key_free_function_simple
	);
}

size_t string_hash_function(void const* data) {
	char const* string = data;
	
	if (string == NULL) {
		return 0;
	}
	
	size_t hash = 0;
	
	while (*string != '\0') {
		hash = (hash * 31) + *string;
		string ++;
	}
	
	return hash;
}

bool string_equal_function(void const* a, void const* b) {
	return strcmp(a, b) == 0;
}

void* string_copy_function(void const* str, HashMapMallocFunction* malloc_function) {
	size_t size = strlen(str) + 1;
	void* new = malloc_function(size);
	memcpy(new, str, size);
	return new;
}

void hash_map_key_free_function_simple(void* key, HashMapFreeFunction* free_function) {
	free_function(key);
}

HashMap* pointer_key_hash_map_new() {
	return pointer_key_custom_hash_map_new(malloc, free);
}

HashMap* pointer_key_custom_hash_map_new(HashMapMallocFunction* malloc_function, HashMapFreeFunction* free_function) {
	return hash_map_new(
		0.75, 16,
		malloc_function, free_function,
		pointer_hash_function, pointer_equal_function,
		pointer_copy_function, pointer_free_function
	);
}

size_t pointer_hash_function(void const* ptr) {
	return ((size_t)ptr) >> 2;
}

bool pointer_equal_function(void const* a, void const* b) {
	return a == b;
}

void* pointer_copy_function(void const* ptr, HashMapMallocFunction* malloc_function) {
	(void)malloc_function;
	return (void*)ptr;
}

void pointer_free_function(void* ptr, HashMapFreeFunction* free_function) {
	(void)ptr;
	(void)free_function;
}
