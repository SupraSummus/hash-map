#include "hash_map.h"

/**
 * types
 */

typedef struct _Entry Entry;
struct _Entry {
	Entry* next;
	void* key;
	void* value;
};

struct _HashMap {
	HashMapMallocFunction* malloc_function;
	HashMapFreeFunction* free_function;
	
	HashMapKeyHashFunction* key_hash_function;
	HashMapKeyEqualFunction* key_equal_function;
	HashMapKeyCopyFunction* key_copy_function;
	HashMapKeyFreeFunction* key_free_function;
	
	float load_factor;
	size_t minimal_size;
	
	Entry** array;
	size_t array_size;
	size_t number_of_elements;
};

/**
 * private function declarations
 */

static Entry** _pointer_to_bucket(
	Entry**, size_t,
	HashMapKeyHashFunction*,
	void const* key
);

static Entry** _pointer_to_entry(
	Entry**, size_t,
	HashMapKeyHashFunction*, HashMapKeyEqualFunction*,
	void const* key
);

static Entry** _container_new(size_t, HashMapMallocFunction*);
static void _container_free(Entry**, size_t, HashMapKeyFreeFunction*, HashMapFreeFunction*);

static Entry** _rehash(
	Entry**, size_t,
	HashMapKeyHashFunction*, HashMapMallocFunction*, HashMapFreeFunction*,
	size_t
);

static void _add_entry(
	Entry**, size_t,
	HashMapKeyHashFunction*,
	Entry*
);

static void hash_map_rehash_if_needed(HashMap*);

/**
 * implementation of private functions
 */

static Entry** _pointer_to_bucket(
	Entry** container, size_t container_size,
	HashMapKeyHashFunction* key_hash_function,
	void const* key
) {
	size_t hash = key_hash_function(key);
	return &container[hash % container_size];
}

static Entry** _pointer_to_entry(
	Entry** container, size_t container_size,
	HashMapKeyHashFunction* key_hash_function, HashMapKeyEqualFunction* key_equal_function,
	void const* key
) {
	Entry** pointer_to_entry = _pointer_to_bucket(
		container, container_size,
		key_hash_function,
		key
	);
	
	/* not found - bucket is empty */
	if (*pointer_to_entry == NULL) {
		return pointer_to_entry;
	}
	
	while (!key_equal_function((*pointer_to_entry)->key, key)) {
		pointer_to_entry = &((*pointer_to_entry)->next);
		
		/* crawled to the end of the list */
		if (*pointer_to_entry == NULL) {
			return pointer_to_entry;
		}
	}
	
	return pointer_to_entry;
}

static Entry** _container_new(size_t size, HashMapMallocFunction* malloc_function) {
	Entry** container = malloc_function(sizeof(Entry*) * size);
	for (size_t i = 0; i < size; i ++) {
		container[i] = NULL;
	}
	return container;
}

static void _container_free(
	Entry** container, size_t container_size,
	HashMapKeyFreeFunction* key_free_function, HashMapFreeFunction* free_function
) {
	// for each bucket
	for (size_t i = 0; i < container_size; i ++) {
		// go through list
		while (container[i] != NULL) {
			// get next element
			Entry* old = container[i];
			container[i] = old->next;
			
			// free
			key_free_function(old->key, free_function);
			free_function(old);
		}
	}
	
	free_function(container);
}

static Entry** _rehash(
	Entry** container, size_t container_size,
	HashMapKeyHashFunction* key_hash_function, HashMapMallocFunction* malloc_function, HashMapFreeFunction* free_function,
	size_t new_size
) {
	Entry** new_container = _container_new(new_size, malloc_function);
	
	// for each bucker
	for (size_t i = 0; i < container_size; i ++) {
		// go through list
		while (container[i] != NULL) {
			// get next element
			// detach element from beginning of the list
			Entry* old = container[i];
			container[i] = old->next;
			
			old->next = NULL;
			
			// insert element to new container
			_add_entry(
				new_container, new_size,
				key_hash_function,
				old
			);
		}
	}
	
	free_function(container);
	
	return new_container;
}

static void _add_entry(
	Entry** container, size_t container_size,
	HashMapKeyHashFunction* key_hash_function,
	Entry* entry
) {
	Entry** bucket = _pointer_to_bucket(
		container, container_size,
		key_hash_function,
		entry->key
	);
	
	entry->next = *bucket;
	*bucket = entry;
}

static void hash_map_rehash_if_needed(HashMap* map) {
	size_t new_size = map->array_size;
	
	if (map->number_of_elements > map->array_size * map->load_factor) {
		new_size = map->array_size*2;
	}
	
	if (map->number_of_elements < map->array_size * map->load_factor/2) {
		new_size = map->array_size/2;
		if (new_size < map->minimal_size) {
			new_size = map->minimal_size;
		}
	}
	
	if (new_size != map->array_size) {
		map->array = _rehash(
			map->array, map->array_size,
			map->key_hash_function, map->malloc_function, map->free_function,
			new_size
		);
		map->array_size = new_size;
	}
}

/**
 * implementation of public functions
 */

HashMap* hash_map_new(
	float load_factor,
	size_t minimal_size,
	HashMapMallocFunction* malloc_function,
	HashMapFreeFunction* free_function,
	HashMapKeyHashFunction* key_hash_function,
	HashMapKeyEqualFunction* key_equal_function,
	HashMapKeyCopyFunction* key_copy_function,
	HashMapKeyFreeFunction* key_free_function
) {
	HashMap* map = malloc_function(sizeof(HashMap));
	if (map == NULL) {
		return NULL;
	}
	
	map->malloc_function = malloc_function;
	map->free_function = free_function;
	map->key_hash_function = key_hash_function;
	map->key_equal_function = key_equal_function;
	map->key_copy_function = key_copy_function;
	map->key_free_function = key_free_function;
	map->load_factor = load_factor;
	map->minimal_size = minimal_size;
	map->number_of_elements = 0;
	map->array_size = map->minimal_size;
	map->array = _container_new(map->array_size, map->malloc_function);
	
	return map;
}

void hash_map_free(HashMap* map) {
	if (map == NULL) {
		return;
	}
	
	_container_free(map->array, map->array_size, map->key_free_function, map->free_function);
	map->free_function(map);
}

bool hash_map_has(HashMap const* map, void const* key) {
	Entry** pointer_to_entry = _pointer_to_entry(
		map->array, map->array_size,
		map->key_hash_function, map->key_equal_function,
		key
	);
	return *pointer_to_entry != NULL;
}

void hash_map_set(HashMap* map, void const* key, void* value) {
	Entry** pointer_to_entry = _pointer_to_entry(
		map->array, map->array_size,
		map->key_hash_function, map->key_equal_function,
		key
	);
	
	if (*pointer_to_entry == NULL) {
		*pointer_to_entry = map->malloc_function(sizeof(Entry));
		(*pointer_to_entry)->next = NULL;
		(*pointer_to_entry)->key = map->key_copy_function(key, map->malloc_function);
		(*pointer_to_entry)->value = value;
		map->number_of_elements ++;
		hash_map_rehash_if_needed(map);
	} else {
		(*pointer_to_entry)->value = value;
	}
}

void* hash_map_get(HashMap const* map, void const* key) {
	Entry** pointer_to_entry = _pointer_to_entry(
		map->array, map->array_size,
		map->key_hash_function, map->key_equal_function,
		key
	);
	
	if (*pointer_to_entry == NULL) {
		return NULL;
	}
	return (*pointer_to_entry)->value;
}

void hash_map_remove(HashMap* map, void const* key) {
	Entry** pointer_to_entry = _pointer_to_entry(
		map->array, map->array_size,
		map->key_hash_function, map->key_equal_function,
		key
	);
	
	if (*pointer_to_entry == NULL) {
		return;
	}
	Entry* to_remove = *pointer_to_entry;
	*pointer_to_entry = to_remove->next;
	map->key_free_function(to_remove->key, map->free_function);
	map->free_function(to_remove);
	map->number_of_elements--;
	
	hash_map_rehash_if_needed(map);
}

size_t hash_map_elements_count(HashMap const* map) {
	return map->number_of_elements;
}

void hash_map_for_every(HashMap* map, HashMapForEveryFunction* callback, void* context_data) {
	// for each bucket
	for (size_t i = 0; i < map->array_size; i ++) {
		// go through list
		Entry* entry = map->array[i];
		while (entry != NULL) {
			/* call */
			callback(entry->key, entry->value, context_data);
			
			/* get next element */
			entry = entry->next;
		}
	}
}
